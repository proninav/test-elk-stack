package com.sonedo.testelkstack.web

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

@RestController
object PatientHandler {

    private val logger: Logger = LoggerFactory.getLogger(PatientHandler.javaClass)

    @GetMapping("/")
    @ResponseBody
    fun getPatients(): String {
        logger.info("GET request")
        logger.debug("GET debug request")
        return "Hello world"
    }
}