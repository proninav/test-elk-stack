package com.sonedo.testelkstack

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestElkStackApplication

fun main(args: Array<String>) {
    runApplication<TestElkStackApplication>(*args)
}
