FROM maven:3-openjdk-8

COPY . /opt/test-elk-stack/src

WORKDIR /opt/test-elk-stack/src

RUN mvn clean install

WORKDIR /opt/test-elk-stack

CMD ["cp", "/opt/test-elk-stack/src/target/test-elk-stack.jar", "/opt/test-elk-stack"]

CMD ["java", "-jar", "src/target/test-elk-stack.jar"]